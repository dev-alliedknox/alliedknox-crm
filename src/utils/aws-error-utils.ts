import { IObjectTemplate } from 'allied-kernel';

const getQueueStatusCode = (error: IObjectTemplate): number => {

    if (error.statusCode) 
        return error.statusCode;

    return 502;
}

export { getQueueStatusCode }; 