import { AuthError } from "../errors/auth-error";

const hasPermission = (perms, method): boolean => {

    return perms.length > 0 && (method == 'GET' && perms[0].canRead || method != 'GET' && perms[0].canWrite);
}

export const verifyPermissions = (req, res, next): any => {
    let perms  = req.permissions;
    // console.log('PERMS', perms)
    // console.log('METHOD', req.method)
    // console.log('ORIGINALURL', req.originalUrl)
    let permissions = perms.filter((permission) => req.originalUrl.includes(permission.resource));
    if (!hasPermission(permissions, req.method)) throw new AuthError('Não autorizado.', 403);

    next();
}