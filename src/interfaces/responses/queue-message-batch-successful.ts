
interface IQueueMessageBatchSuccessful {

    Id: string;
    MessageId: string;
    MD5OfMessageBody: string;
}

export default IQueueMessageBatchSuccessful;