
interface IQueueMessageBatchFailed {

    Id: string;
    SenderFault: boolean;
    Code: string;
    Message: string;
}

export default IQueueMessageBatchFailed;