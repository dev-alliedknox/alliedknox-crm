
interface ILambdaInvokeResponse {

    StatusCode: number;
    Payload: any;
}

export default ILambdaInvokeResponse;