import IQueueMessageBatchFailed from './queue-message-batch-failed';
import IQueueMessageBatchSuccessful from './queue-message-batch-successful';

interface IQueueMessageBatchResponse {

    Failed: IQueueMessageBatchFailed[];
    Successful: IQueueMessageBatchSuccessful[];
}

export default IQueueMessageBatchResponse;