import LambdaInvokeInvocationTypeEnum from '../../enums/lambda-invoke-invocation-type';
import LambdaInvokeLogTypeEnum from '../../enums/lambda-invoke-log-type';

interface ILambdaInvokeRequest {

    FunctionName: string;
    InvocationType: LambdaInvokeInvocationTypeEnum;
    LogType: LambdaInvokeLogTypeEnum;
    Payload: any;
}

export default ILambdaInvokeRequest;