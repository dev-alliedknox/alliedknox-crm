
interface IDeleteMessageBatchRequest {

    Id: string;
    ReceiptHandle: string;
}

export default IDeleteMessageBatchRequest;