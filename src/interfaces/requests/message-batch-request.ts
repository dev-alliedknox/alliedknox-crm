import { IObjectTemplate } from 'allied-kernel';

interface IMessageBatchRequest {

    Id: string;
    MessageBody: string;
    DelaySeconds?: number;
    MessageAttributes?: IObjectTemplate;
    MessageDeduplicationId?: string;
    MessageGroupId?: string;
}

export default IMessageBatchRequest;
