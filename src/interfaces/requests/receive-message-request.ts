
interface IReceiveMessageRequest {
    
    MaxNumberOfMessages: number;
    AttributeNames?: string[];
    MessageAttributeNames?: string[];
    VisibilityTimeout?: number;
    WaitTimeSeconds?: number;
    ReceiveRequestAttemptId?: string;
}

export default IReceiveMessageRequest;