import { Router } from 'express';
import * as controller from '../controllers/controller';
import { asyncMiddleware } from 'allied-kernel';
import { swaggerSetup, swaggerUi } from '../configs/swagger';
import { verifyJWT } from '../utils/jwtValidation';
import * as cors from 'cors';
import { verifyPermissions } from '../utils/verifyPermissions';

const router = Router();

router.options('*', cors());

router.use('/', swaggerUi.serve);
router.get('/api-docs', swaggerSetup);

router.use('/', [ verifyJWT, verifyPermissions ]);
router.get('/messages', asyncMiddleware(controller.listMessages));
router.post('/messages', asyncMiddleware(controller.messageDevices));
router.post('/messages/blink', asyncMiddleware(controller.blinkDevices));
router.put('/devices/lock', asyncMiddleware(controller.lockDevices));
router.put('/devices/unlock', asyncMiddleware(controller.unlockDevices));
router.post('/devices/pin', asyncMiddleware(controller.getPin));

export default router;