import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import routes from './routes';
import { errorHandler, requestLog } from 'allied-kernel';
const serverless = require('serverless-http');
const awsXRay = require('aws-xray-sdk');
const app = express();
import * as cors from 'cors';

app.use(bodyParser.json());
app.use(compression());
app.use(awsXRay.express.openSegment('CrmSegment'));
app.use('/', requestLog);
app.use(cors());
app.use(routes);
app.use(awsXRay.express.closeSegment());
app.use(errorHandler);

module.exports.handler = serverless(express().use('/crm', app));
