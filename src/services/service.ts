import Device from '../clients/Device';
import { ValidateError } from "../errors/ValidateError";
import { AccessCheckWhitelistService } from '../services/AccessCheckWhitelistService';
import { AccessMessageListService } from '../services/AccessMessageListService';
import { AccessRelacionamentoSoudi } from '../services/AccessRelacionamentoSoudi';
import { AccessCheckInfoService } from '../services/AccessCheckInfoService';
import { AccessUpdateStatusKgService } from '../services/AccessUpdateStatusKgService'
import { gravaRequest, gravaResponse } from '../middlewares/gravaRequestResponse';
import MessageDeviceIn from '../models/MessageDeviceIn';
import MessageDeviceOut from '../models/MessageDeviceOut';
import BlinkDeviceIn from '../models/BlinkDeviceIn';
import BlinkDeviceOut from '../models/BlinkDeviceOut';
import LockDeviceIn from '../models/LockDeviceIn';
import LockDeviceOut from '../models/LockDeviceOut';
import UnlockDeviceIn from '../models/UnlockDeviceIn';
import UnlockDeviceOut from '../models/UnlockDeviceOut';
import KgStatus from '../models/KgStatus';
import DevicePin from '../models/DevicePin';
import OfflinePin from '../models/OfflinePin';
import { isNullOrUndefined } from 'util';
import { USERNAME } from '../utils/jwtValidation';
import QueueService from '../services/queue-service';
//import { runInNewContext } from 'vm';

class Service {
    private device: Device;
    private accessCheckWhitelistService: AccessCheckWhitelistService;
    private accessMessageListService: AccessMessageListService;
    private accessCheckInfoService: AccessCheckInfoService;
    private accessUpdateStatusKgService: AccessUpdateStatusKgService;
    private accessRelacionamentoSoudi: AccessRelacionamentoSoudi;
    private usuario = USERNAME;
    private queueService;


    public constructor() {
        this.device = new Device();
        this.accessCheckWhitelistService = new AccessCheckWhitelistService();
        this.accessMessageListService = new AccessMessageListService();
        this.accessCheckInfoService = new AccessCheckInfoService();
        this.accessUpdateStatusKgService = new AccessUpdateStatusKgService();
        this.accessRelacionamentoSoudi = new AccessRelacionamentoSoudi();
        this.queueService = new QueueService(String(process.env.SOUDI_HERCULES_QUEUE), { region: String(process.env.REGIAO) });
    }


    //  METODOS UTILIZADOS PELOS ENDPOINTS
    public async listMessages() {
        let res = await this.accessMessageListService.getMessageList();
        return res;
    }

    public async getSoudi() {
        let res = await this.accessRelacionamentoSoudi.getRelacionamento();
        return res[0];
    }

    private async getMessage(param: number) {
        let message = await this.accessMessageListService.getMessageById(param);

        if (message == '')
            throw new ValidateError('Não existe mensagem válida correspondente ao messageId informado.');

        return message[0].mensagem;
    }

    private async getTags(param: number) {
        let tags = await this.accessMessageListService.getTags(param);

        return tags;
    }

    private async getMsgInfo(tags: any, cpf: string) {
        let msgInfo = await this.accessMessageListService.getMsgInfo(tags, cpf);

        return msgInfo;
    }

    private async formatMessageTags(msgInfo: any, msg: any) {
        msgInfo = msgInfo[0];
        let mensagemOk = msg;

        Object.keys(msgInfo).forEach(item => {
            mensagemOk = mensagemOk.replace(`[${item}]`, msgInfo[item]);
        });

        return mensagemOk;
    }

    private async formatMessageBody(body: any, msg: any) {
        let mensagemOk = msg;

        Object.keys(body).forEach(item => {
            mensagemOk = mensagemOk.replace(`[${item}]`, body[item]);
        });

        if (!isNullOrUndefined(msg.dtHrAuth) && !isNullOrUndefined(msg.dtVenc) && !isNullOrUndefined(msg.parcelaAberta) && !isNullOrUndefined(msg.valorCompra)) {
            return msg;
        }
        return mensagemOk;
    }

    private async checkInfoCpf(cpf: string) {
        let checkInfo = await this.accessCheckInfoService.getInfoByCpf(cpf);
        JSON.parse(JSON.stringify(checkInfo));

        if (checkInfo == '')
            throw new ValidateError('CPF não encontrado/inválido.');

        return checkInfo;
    }

    private async getCpfByImei(imei: string) {
        let cpf = await this.accessCheckInfoService.getCpfByImei(imei);
        JSON.parse(JSON.stringify(cpf));

        if (cpf == '')
            throw new ValidateError('Não existe CPF associado ao IMEI/SN informado.');

        return cpf[0].cpf_cnpj;
    }

    private async checkWhitelist(param: string[]) {
        let checkWhitelist = await this.accessCheckWhitelistService.checkWhitelist(param);
        JSON.parse(JSON.stringify(checkWhitelist));

        if (checkWhitelist.length == param.length)
            throw new ValidateError('Para este CPF não existem dispositivos aptos a receber mensagens e/ou ações.');

        return checkWhitelist;
    }

    private async updateStatusKg(serial: string, status: string) {
        await this.accessUpdateStatusKgService.updateStatusKg(serial, status);
    }

    private async updateDatabase(serialList: string[]) {
        let deviceStatusManagementList = [{}];
        for (let i = 0; i < serialList.length; i++) {
            let kgStatus = new KgStatus(serialList[i]);
            let status;
            //    do{
            let result = await this.device.getKgStatus(kgStatus);
            // console.log('RESULT: ', result);
            if (result.deviceList) {
                status = result.deviceList[0].status;
                deviceStatusManagementList[i] = { "serial": serialList[i], "status": status };
                await this.updateStatusKg(serialList[i], status);
            }
            //    }while (status == 'StartingReminder' || status == 'StoppingReminder' ||  
            //            status == 'Locking' || status == 'Unlocking' || status == 'Completing' );             
        }
        return deviceStatusManagementList;
    }

    private async getSeriais(data: any) {
        let serial;
        if (data.imei == '')
            serial = await this.checkInfoCpf(data.cpf);
        else
            serial = [data.imei];

        return serial;
    }

    private async getSerialList(data: any, serial: any) {
        let serialList;
        if (data.imei == '')
            serialList = serial.map(serial => serial.serial);
        else
            serialList = serial;

        let whiteList = await this.checkWhitelist(serialList);
        whiteList = whiteList.map(item => item.serialNumber);
        serialList = serialList.filter(item => !whiteList.includes(item));

        return serialList;
    }

    private async buildMessage(data: any) {
        let mensagem = await this.getMessage(data.messageId);
        let tags = await this.getTags(data.messageId);
        console.log('==tagsANTES=='); console.log(tags);
        if (tags != '') {
            tags = tags.map(item => item.descricaoTags);
            console.log('==tags=='); console.log(tags);
            tags = tags.filter(item => !item.includes('valorCompra'));
            tags = tags.filter(item => !item.includes('dtVenc'));
            tags = tags.filter(item => !item.includes('parcelaAberta'));
            console.log('==tagsFILTRADA=='); console.log(tags);
            let msgInfo;
            if (data.cpf != '') {
                msgInfo = await this.getMsgInfo(tags, data.cpf);
                console.log('==msginfoCPF=='); console.log(msgInfo);
                if (msgInfo[0].hasOwnProperty('nome_cliente')) {
                    let nome = msgInfo[0].nome_cliente;
                    let newNome = nome.substr(0, nome.indexOf(' '))
                    console.log('==primeiroNome=='); console.log(newNome);
                    msgInfo[0].nome_cliente = newNome;
                    //console.log('==msginfoDEPOIS==');console.log(msgInfo);
                }
            } else {
                data.cpf = await this.getCpfByImei(data.imei);
                console.log('==msg.CPF=='); console.log(data.cpf);
                msgInfo = await this.getMsgInfo(tags, data.cpf);
                console.log('==msginfoIMEI=='); console.log(msgInfo);
                if (msgInfo[0].hasOwnProperty('nome_cliente')) {
                    let nome = msgInfo[0].nome_cliente;
                    let newNome = nome.substr(0, nome.indexOf(' '))
                    console.log('==primeiroNome=='); console.log(newNome);
                    msgInfo[0].nome_cliente = newNome;
                    //console.log('==msginfoDEPOIS==');console.log(msgInfo);
                }
            }
            if (msgInfo == '')
                throw new ValidateError('Não existem informações suficientes para compor a mensagem');

            mensagem = await this.formatMessageTags(msgInfo, mensagem);
            console.log('==msgTags=='); console.log(mensagem);
        }
        mensagem = await this.formatMessageBody(data, mensagem);
        console.log('==msgBody=='); console.log(mensagem);
        return mensagem;
    }


    //  ENDPOINTS

    public async messageDevices(data: any) {
        const acao = 'Mensagem';
        console.log('===SendMessage===');
        let msg = new MessageDeviceIn(data);
        //BUSCA LISTA DE SERIAIS COM CPF INFORMADO  
        console.log('===getSeriais(if_CPF)===');
        let serial = await this.getSeriais(msg);
        //EXCLUI SERIAIS QUE ESTÃO NA WHITELIST DA LISTA DE SERIAIS A SER ENVIADA
        console.log('===filterWhitelist===');
        let serialList = await this.getSerialList(msg, serial);
        //BUSCA MENSAGEM NO BANCO E FORMATA COM VALORES INFORMADOS (MESSAGEID)
        console.log('===buildMessage==');
        let mensagem = await this.buildMessage(msg);
        //GRAVA EM BANCO DADOS DA REQUISIÇÃO E RESGATA TRANSACTIONID   
        console.log('===gravaRequest===');
        let transactionId = await gravaRequest(msg.cpf, this.usuario, acao, mensagem, msg, serialList);
        //BUSCA RELACIONAMENTO SOUDI (EMAIL,TELEFONE)
        let soudi = await this.getSoudi();
        let email_soudi = soudi.email_soudi;
        let telefone_soudi = soudi.telefone_soudi.replace(/[() -]/g, '');
        //ENVIA REQUISIÇÃO (KG -> KNOX)
        let msgOut = new MessageDeviceOut(serialList, email_soudi, telefone_soudi, mensagem);
        console.log('***messageDevices***');
        let res = await this.device.messageDevices(msgOut);
        //GRAVA RESPOSTA DA REQUISIÇÃO        
        let response = JSON.stringify(res);
        console.log('===gravaResponse===');
        await gravaResponse(transactionId[0].id, response);
        //ATUALIZA BANCO COM STATUS ATUAL KG P/ SERIAIS ALTERADOS
        console.log('===updateDatabase===');
        let listToUpdate = await this.updateDatabase(serialList);
        //RETURN
        console.log('===processo_terminado===');
        return res;
    }


    public async blinkDevices(data: any) {
        const acao = 'Blink';
        console.log('===BlinkMessage===');
        const blink = new BlinkDeviceIn(data);
        //BUSCA LISTA DE SERIAIS COM CPF INFORMADO
        console.log('===getSeriais(if_CPF)===');
        let serial = await this.getSeriais(blink);
        //EXCLUI SERIAIS QUE ESTÃO NA WHITELIST DA LISTA DE SERIAIS A SER ENVIADA   
        console.log('===filterWhitelist===');
        let serialList = await this.getSerialList(blink, serial);
        //BUSCA MENSAGEM NO BANCO E FORMATA COM VALORES INFORMADOS (MESSAGEID)  
        console.log('===buildMessage==');
        let mensagem = await this.buildMessage(blink);
        //GRAVA EM BANCO DADOS DA REQUISIÇÃO E RESGATA TRANSACTIONID
        console.log('===gravaRequest===');
        let transactionId = await gravaRequest(blink.cpf, this.usuario, acao, mensagem, blink, serialList);
        //BUSCA RELACIONAMENTO SOUDI (EMAIL,TELEFONE)
        let soudi = await this.getSoudi();
        let email_soudi = soudi.email_soudi;
        let telefone_soudi = soudi.telefone_soudi.replace(/[() -]/g, '');
        //ENVIA REQUISIÇÃO (KG -> KNOX)        
        const blinkOut = new BlinkDeviceOut(serialList, email_soudi, telefone_soudi, mensagem, blink.blinkInterval);
        console.log('***blinkDevices***');
        let res = await this.device.blinkDevices(blinkOut);
        //GRAVA RESPOSTA DA REQUISIÇÃO 
        let response = JSON.stringify(res);
        console.log('===gravaResponse===');
        await gravaResponse(transactionId[0].id, response);
        //ATUALIZA BANCO COM STATUS ATUAL KG P/ SERIAIS ALTERADOS
        console.log('===updateDatabase===');
        let listToUpdate = await this.updateDatabase(serialList);
        //ENVIA SERIAIS E RESPECTIVOS STATUS PARA FILA, PARA SEREM ATUALIZADOS PELO SERVIÇO 'deviceStatusManagement' 
        console.log('===deviceStatusManagementQueue===');
        console.log('listToUpdate', listToUpdate);
        await this.queueService.sendMessage(listToUpdate, 60);
        //RETURN
        console.log('===processo_terminado===');
        return res;
    }


    public async lockDevices(data: any) {
        const acao = 'Bloqueio';
        console.log('===LockDevice===');
        let lock = new LockDeviceIn(data);
        //BUSCA LISTA DE SERIAIS COM CPF INFORMADO
        console.log('===getSeriais(if_CPF)===');
        let serial = await this.getSeriais(lock);
        //EXCLUI SERIAIS QUE ESTÃO NA WHITELIST DA LISTA DE SERIAIS A SER ENVIADA  
        console.log('===filterWhitelist===');
        let serialList = await this.getSerialList(lock, serial);
        //BUSCA MENSAGEM NO BANCO E FORMATA COM VALORES INFORMADOS (MESSAGEID)
        console.log('===buildMessage==');
        let mensagem = await this.buildMessage(lock);
        //GRAVA EM BANCO DADOS DA REQUISIÇÃO E RESGATA TRANSACTIONID
        console.log('===gravaRequest===');
        let transactionId = await gravaRequest(lock.cpf, this.usuario, acao, mensagem, lock, serialList);
        //BUSCA RELACIONAMENTO SOUDI (EMAIL,TELEFONE)
        let soudi = await this.getSoudi();
        let email_soudi = soudi.email_soudi;
        let telefone_soudi = soudi.telefone_soudi.replace(/[() -]/g, '');
        //ENVIA REQUISIÇÃO (KG -> KNOX)
        let lockOut = new LockDeviceOut(serialList, email_soudi, telefone_soudi, mensagem);
        console.log('***lockDevices***');
        let res = await this.device.lockDevices(lockOut);
        //GRAVA RESPOSTA DA REQUISIÇÃO 
        let response = JSON.stringify(res);
        console.log('===gravaResponse===');
        await gravaResponse(transactionId[0].id, response);
        //ATUALIZA BANCO COM STATUS ATUAL KG P/ SERIAIS ALTERADOS
        console.log('===updateDatabase===');
        let listToUpdate = await this.updateDatabase(serialList);
        //ENVIA SERIAIS E RESPECTIVOS STATUS PARA FILA, PARA SEREM ATUALIZADOS PELO SERVIÇO 'deviceStatusManagement' 
        console.log('===deviceStatusManagementQueue===');
        console.log('listToUpdate', listToUpdate);
        await this.queueService.sendMessage(listToUpdate, 60);
        //RETURN
        console.log('===processo_terminado===');
        return res;
    }


    public async unlockDevices(data: any) {
        const acao = 'Desbloqueio';
        console.log('===UnlockDevice===');
        let unlock = new UnlockDeviceIn(data);
        //BUSCA LISTA DE SERIAIS COM CPF INFORMADO
        console.log('===getSeriais(if_CPF)===');
        let serial = await this.getSeriais(unlock);
        //EXCLUI SERIAIS QUE ESTÃO NA WHITELIST DA LISTA DE SERIAIS A SER ENVIADA   
        console.log('===filterWhitelist===');
        let serialList = await this.getSerialList(unlock, serial);
        //GRAVA EM BANCO DADOS DA REQUISIÇÃO E RESGATA TRANSACTIONID        
        console.log('===gravaRequest===');
        let transactionId = await gravaRequest(unlock.cpf, this.usuario, acao, '', unlock, serialList);
        //ENVIA REQUISIÇÃO (KG -> KNOX)        
        let unlockOut = new UnlockDeviceOut(serialList);
        console.log('***unlockDevices***');
        let res = await this.device.unlockDevices(unlockOut);
        //GRAVA RESPOSTA DA REQUISIÇÃO        
        let response = JSON.stringify(res);
        console.log('===gravaResponse===');
        await gravaResponse(transactionId[0].id, response);
        //ATUALIZA BANCO COM STATUS ATUAL KG P/ SERIAIS ALTERADOS
        console.log('===updateDatabase===');
        let listToUpdate = await this.updateDatabase(serialList);
        //ENVIA SERIAIS E RESPECTIVOS STATUS PARA FILA, PARA SEREM ATUALIZADOS PELO SERVIÇO 'deviceStatusManagement' 
        console.log('===deviceStatusManagementQueue===');
        console.log('listToUpdate', listToUpdate);
        await this.queueService.sendMessage(listToUpdate, 60);
        //RETURN
        console.log('===processo_terminado===');
        return res;
    }


    public async getPin(data: any) {
        const acao = 'GetPin';
        console.log('===GETPIN===');
        let res; let transactionId;
        //BUSCA STATUS KG DO DISPOSITIVO
        let kgStatus = new KgStatus(data.serial);
        console.log('===getKgStatus===');
        let status = await this.device.getKgStatus(kgStatus);
        if (status.totalCount == 0) {
            throw new ValidateError('Dispositivo não encontrado.');
        }
        //CHAMA ENDPOINT DE ACORDO COM VERSAO DO ANDROID, E GRAVA A REQUEST
        if (status.deviceList[0].isDeviceSupportHotp == true) {
            let offlinePin = new OfflinePin(data);
            console.log('===gravaRequest===');
            transactionId = await gravaRequest('', this.usuario, acao, 'CPF não informado nesta funcionalidade', offlinePin, [offlinePin.deviceUid]);
            console.log('***getOfflinePin***');
            res = await this.device.getOfflinePin(offlinePin);
        } else {
            let devicePin = new DevicePin(data);
            console.log('===gravaRequest===');
            transactionId = await gravaRequest('', this.usuario, acao, 'CPF não informado nesta funcionalidade', devicePin, [devicePin.deviceUid]);
            console.log('***getDevicePin***');
            res = await this.device.getDevicePin(devicePin);
        }
        //GRAVA RESPOSTA DA REQUISIÇÃO        
        let response = JSON.stringify(res);
        console.log('===gravaResponse===');
        await gravaResponse(transactionId[0].id, response);
        //RETURN
        console.log('===processo_terminado===');
        return res;
    }

}
export default Service;