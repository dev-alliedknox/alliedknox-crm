import { KnexSingleton } from './KnexSingleton'

class AccessUpdateStatusKgService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public updateStatusKg = async (serial: string, status: string) => {
        await this.connection('linxMovimentoSerial')
            .where('serial', '=', serial)
            .update('status_kg', status);
    }

}

export { AccessUpdateStatusKgService }