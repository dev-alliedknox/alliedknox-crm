import { KnexSingleton } from './KnexSingleton'

class AccessTokenService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public getTokenKG = async () => {
        return await this.connection.select('knoxPublicToken.public_token_kg')
            .from('knoxPublicToken')
            .orderBy('id', 'desc')
            .first();
    }

    public getTokenKDP = async () => {
        return await this.connection.select('knoxPublicToken.public_token_kdp')
            .from('knoxPublicToken')
            .orderBy('id', 'desc')
            .first();
    }
}

export { AccessTokenService }