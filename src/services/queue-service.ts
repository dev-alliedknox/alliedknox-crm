
import { SQS } from 'aws-sdk';
//import { any } from 'allied-kernel';
import { getQueueStatusCode } from '../utils/aws-error-utils';
import QueueIntegrationError from '../errors/queue-integration-error';
import IMessageBatchRequest from '../interfaces/requests/message-batch-request';
import IReceiveMessageRequest from '../interfaces/requests/receive-message-request';
import IDeleteMessageBatchRequest from '../interfaces/requests/delete-message-batch-request';
import IQueueMessageBatchResponse from '../interfaces/responses/queue-message-batch-response';

class QueueService {

    private configs: any;
    private queueUrl: string;
    private sqs: SQS;

    public constructor(queueUrl: string, configs: any = {}) {

        this.configs = configs;
        this.queueUrl = queueUrl;
        this.sqs = new SQS(this.configs);
    }

    public deleteMessage(receiptHandle: string): any {

        let params = {

            QueueUrl: this.queueUrl,
            ReceiptHandle: receiptHandle
        }

        return this.sqs.deleteMessage(params).promise()
            .then((data): any => { return data })
            .catch((err): void => { throw new QueueIntegrationError(err.message, getQueueStatusCode(err))});
    }

    public deleteMessageBatch(entries: IDeleteMessageBatchRequest[]): any {

        let params = { 

            Entries: entries,
            QueueUrl: this.queueUrl
        };

        return this.sqs.deleteMessageBatch(params).promise()
            .then((data): any => { return data })
            .catch((err): void => { throw new QueueIntegrationError(err.message, getQueueStatusCode(err))});
    }

    public async sendMessage(message: any, delaySeconds: number = 0, messageAtributes: any = {}): Promise<any> {

        let params = {

            MessageBody: JSON.stringify(message),
            QueueUrl: this.queueUrl,
            DelaySeconds: delaySeconds,
            MessageAttributes: messageAtributes
        }

        return this.sqs.sendMessage(params).promise()
            .then((data): any => { return data })
            .catch((err): any => { throw new QueueIntegrationError(err.message, getQueueStatusCode(err))});
    }

    public async sendMessageBatch(entries: IMessageBatchRequest[]): Promise<IQueueMessageBatchResponse> {

        let params: any = { 

            Entries: entries,
            QueueUrl: this.queueUrl
        };

        return this.sqs.sendMessageBatch(params).promise()
            .catch((err): any => { throw new QueueIntegrationError(err.message, getQueueStatusCode(err)) });
    }

    public receiveMessage(receiveMessageRequest: IReceiveMessageRequest): any {
        
        let params = {

            QueueUrl: this.queueUrl,
            AttributeNames: receiveMessageRequest.AttributeNames,
            MaxNumberOfMessages: receiveMessageRequest.MaxNumberOfMessages,
            MessageAttributeNames: receiveMessageRequest.MessageAttributeNames,
            VisibilityTimeout: receiveMessageRequest.VisibilityTimeout,
            WaitTimeSeconds: receiveMessageRequest.WaitTimeSeconds,
            ReceiveRequestAttemptId: receiveMessageRequest.ReceiveRequestAttemptId
        }

        return this.sqs.receiveMessage(params).promise()
            .then((data): any => { return data })
            .catch((err): void => { throw new QueueIntegrationError(err.message, getQueueStatusCode(err)) });
    }
}

export default QueueService;