import { KnexSingleton } from './KnexSingleton'

class AccessCheckInfoService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public getCpfByImei = async (imei: string) => {
        return await this.connection.select('linxFaturamento.cpf_cnpj')
            .from('linxFaturamento')        
            .innerJoin('linxMovimentoSerial', 'linxFaturamento.transacao', 'linxMovimentoSerial.transacao')                     
            .where('linxMovimentoSerial.serial', imei);               
    }

    public getInfoByCpf = async (cpf: string) => {
        return await this.connection.select('linxMovimentoSerial.serial', 'linxFaturamento.email_cliente', 'linxFaturamento.fone_cliente')
            .from('linxMovimentoSerial')        
            .innerJoin('linxFaturamento', 'linxMovimentoSerial.transacao', 'linxFaturamento.transacao')                     
            .where('linxFaturamento.cpf_cnpj', cpf)
            .andWhere('linxMovimentoSerial.status_kdp', 'Verified')
        //    .andWhere('linxMovimentoSerial.status_kg', 'Active');        
    }
}

export { AccessCheckInfoService }