import { KnexSingleton } from './KnexSingleton'

class AccessCheckWhitelistService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public checkWhitelist = async (param: string[]) => {
        return await this.connection.select('serialNumber')
            .from('serialWhitelist')
            .whereIn('serialNumber', param)    
            .andWhere('ativo', 1);
    }
}

export { AccessCheckWhitelistService }