import { KnexSingleton } from './KnexSingleton'

class AccessMessageListService {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public getMessageList = async () => {
        return await this.connection.select('*')
            .from('listaMensagens');
    }

    public getMessageById = async (param: any) => {
        return await this.connection.select('listaMensagens.mensagem')
            .from('listaMensagens')
            .where('id', param)
            .andWhere('ativo', '1');
    }

    public getTags = async (param: any) => {
        return await this.connection.select('tagsMensagens.descricaoTags')
            .from('unionMensagensTags')        
            .innerJoin('listaMensagens', 'listaMensagens.id', 'unionMensagensTags.idListaMensagens') 
            .innerJoin('tagsMensagens', 'tagsMensagens.id', 'unionMensagensTags.idTagsMensagens')      
            .where('listaMensagens.id', param);              
    }

    public getMsgInfo = async (tags: any, cpf: string) => {
        return await this.connection.select(tags)
            .from('linxFaturamento ')
            .where('cpf_cnpj ', cpf);
    }

}

export { AccessMessageListService }