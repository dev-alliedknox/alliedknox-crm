import { KnexSingleton } from './KnexSingleton'

class AccessRelacionamentoSoudi {
    private connection;
    constructor () {
        this.connection = KnexSingleton.getInstance(process.env.DBHOST, process.env.DBUSER, Number.parseInt(process.env.DBPORT || '3306'), process.env.DBPASSWORD, process.env.DBNAME).conn;
    }

    public getRelacionamento = async () => {
        return await this.connection.select('*')
            .from('relacionamentoSoudi');        
    }
}

export { AccessRelacionamentoSoudi }