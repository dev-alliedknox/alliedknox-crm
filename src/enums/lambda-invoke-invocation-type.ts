
enum LambdaInvokeInvocationTypeEnum {

    Event = 'Event',
    RequestResponse = 'RequestResponse',
    DryRun = 'DryRun'
}

export default LambdaInvokeInvocationTypeEnum;