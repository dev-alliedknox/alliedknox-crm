
enum LambdaInvokeLogTypeEnum {

    None = 'None',
    Tail = 'Tail'
}

export default LambdaInvokeLogTypeEnum;