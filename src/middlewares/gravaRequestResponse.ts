import { AccessTransactionService } from '../services/AccessTransactionService';
import TransactionRequest from '../models/TransactionRequest';


export async function gravaRequest(cpf: string, usuario: string, acao: string, mensagem: string, request: object, serialList: string[]) {                
    let transactionRequest = new TransactionRequest(cpf, usuario, acao, mensagem, request, serialList);    
    
    this.accessTransactionService = new AccessTransactionService(); 
    await this.accessTransactionService.gravaRequest(transactionRequest); 
    return await this.accessTransactionService.getTransactionId(transactionRequest); 
}

export async function gravaResponse(transactionId: number, response: string) {                
    this.accessTransactionService = new AccessTransactionService(); 
    await this.accessTransactionService.gravaResponse(transactionId, response); 
}