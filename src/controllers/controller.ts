import { Request, Response } from 'express';
import Service from '../services/service';

let header;

export const listMessages = async (req: Request, res: Response) => {
    const service = new Service();         
       
    const response = await service.listMessages();  
    res.send(response);   
    //res.status(200).json(response);
}


export const messageDevices = async (req: Request, res: Response)  => {    
    let head = req.headers;      
    header = head["x-access-token"];    
    const service = new Service();
    let data = req.body;         
 
    const response = await service.messageDevices(data);    
    res.send(response); 
    //res.status(200).json(response);  
} 


export const blinkDevices = async (req: Request, res: Response) => {
    let head = req.headers;      
    header = head["x-access-token"];

    const service = new Service();
    let data = req.body;    
    
    const response = await service.blinkDevices(data);   
    res.send(response); 
    //res.status(200).json(response);
}


export const lockDevices = async (req: Request, res: Response) => {
    let head = req.headers;      
    header = head["x-access-token"];

    const service = new Service();
    let data = req.body;    

    const response = await service.lockDevices(data);
    res.send(response); 
    //res.status(200).json(response);
}


export const unlockDevices = async (req: Request, res: Response) => {
    let head = req.headers;      
    header = head["x-access-token"];

    const service = new Service();
    let data = req.body;    

    const response = await service.unlockDevices(data);
    res.send(response); 
    //res.status(200).json(response);
}


export const getPin = async (req: Request, res: Response) => {
    let head = req.headers;      
    header = head["x-access-token"];

    const service = new Service();
    let data = req.body;    

    const response = await service.getPin(data);
    res.send(response); 
    //res.status(200).json(response);
}


export { header };