import { HttpClient } from 'allied-kernel';
import { KnoxError } from '../errors/KnoxError';
import { header } from '../controllers/controller';

class Device {
    private httpClient: HttpClient;   

    public constructor() {
        this.httpClient = new HttpClient({            
            baseURL: process.env.BASE_URL_CLIENT,            
            headers: {                
                'Accept': 'application/json',
                'Content-Type': 'application/json',                
                'cache-control': 'no-cache',                
                'x-access-token': header
            }                         
        });        
    }


    public async messageDevices(msg: object): Promise<any> {                                
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/devices/sendMessage`, msg)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }

    
    public async blinkDevices(blink: object): Promise<any> {                  
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/devices/blink`, blink)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


    public async lockDevices(lock: object): Promise<any> {        
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/devices/lock`, lock)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


    public async unlockDevices(unlock: object): Promise<any> {
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/devices/unlock`, unlock)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


    public async getKgStatus(kgStatus: object): Promise<any> {
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/devices/list`, kgStatus)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


    public async getOfflinePin(offlinePin: object): Promise<any> {
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/device/getOfflinePin`, offlinePin)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


    public async getDevicePin(devicePin: object): Promise<any> {
        const response = await this.httpClient.post(`${process.env.KG_API_GATEWAY}/device/getPin`, devicePin)
        .catch((err) => { throw new KnoxError(err.response.data.message, err.response.status) });
        return response.data;
    }


}

export default Device;