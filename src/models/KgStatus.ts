
class KgStatus {  
    // REQUIRED
    public pageNum: number;
    public pageSize: number;  
    public search: string;   
  
    
    public constructor(search: string) {
      this.pageNum = 0;   
      this.pageSize = 5;
      this.search = search;
    }
  
  }
  
  export default KgStatus;