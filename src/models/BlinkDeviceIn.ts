import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";


class BlinkDeviceIn implements IValidateRequest{
  // REQUIRED
  public cpf: string;
  public imei: string;
  public dtHrAuth: string;
  public valorCompra: number;
  public messageId: number;      
  public dtVenc: string;  
  public parcelaAberta: number;  
  public blinkInterval: number;

  
  public constructor(data: any) {
    this.cpf = data.cpf || '';
    this.imei = data.imei || '';
    this.dtHrAuth = data.dtHrAuth;
    this.valorCompra = data.valorCompra;
    this.messageId = data.messageId;
    this.dtVenc = data.dtVenc;
    this.parcelaAberta = data.parcelaAberta;    
    this.blinkInterval = data.blinkInterval;

    this.validate();
  }

  public validate() {    
    let emptyCPF: boolean = isNullOrUndefined(this.cpf) || (this.cpf == '');
    let emptyIMEI: boolean = isNullOrUndefined(this.imei) || (this.imei == '');
    let emptyMessageId: boolean = isNullOrUndefined(this.messageId);
    let emptyBlink: boolean = isNullOrUndefined(this.blinkInterval);

    if (emptyCPF) {
      if (emptyIMEI) 
        throw new ValidateError('CPF or IMEI/SN is required.');
    }      
    
    if (emptyMessageId) 
      throw new ValidateError('MessageId is required.');                                 

    if (emptyBlink)
      throw new ValidateError('BlinkInterval is required.');
    else 
      if (this.blinkInterval < 3 || this.blinkInterval > 86400)
        throw new ValidateError('BlinkInterval must be greater than 3 and lesser than 86400.');
   
  }

}

export default BlinkDeviceIn;