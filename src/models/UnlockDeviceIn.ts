import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class UnlockDeviceIn implements IValidateRequest{
  // REQUIRED
  public cpf: string;
  public imei: string;


  public constructor(data: any) {
    this.cpf = data.cpf || '';    
    this.imei = data.imei || '';    

    this.validate();
  }

  
  public validate() {    
    let emptyCPF: boolean = isNullOrUndefined(this.cpf) || (this.cpf == '');
    let emptyIMEI: boolean = isNullOrUndefined(this.imei) || (this.imei == '');
    
    if (emptyCPF) {
      if (emptyIMEI) 
        throw new ValidateError('CPF or IMEI/SN is required.');
    }      

  }

}

export default UnlockDeviceIn;