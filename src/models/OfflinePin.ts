import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class OfflinePin implements IValidateRequest{
  // REQUIRED
  public deviceUid: string;
  public challenge: string;


  public constructor(data: any) {
    this.deviceUid = data.serial || '';    
    this.challenge = data.passKey || ''; 

    this.validate();
  }

  public validate() {    
    let emptyDeviceUid: boolean = isNullOrUndefined(this.deviceUid) || (this.deviceUid == '');
    let emptychallenge: boolean = isNullOrUndefined(this.challenge) || (this.challenge == '');

    if (emptyDeviceUid) 
      throw new ValidateError('Serial is required.');          

    if (emptychallenge) 
      throw new ValidateError('passKey is required.'); 

  }

}

export default OfflinePin;