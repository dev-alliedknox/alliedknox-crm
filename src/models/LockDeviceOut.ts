
class LockDeviceOut {  
    // REQUIRED
    public deviceUid: string[];
    public email: string;
    public tel: string;
    public message: string;      
  
    
    public constructor(serial: string[], email: string, tel: string, message: string) {
      this.deviceUid = serial;   
      this.email = email;
      this.tel = tel;
      this.message = message;    
    }
  
  }

  export default LockDeviceOut;