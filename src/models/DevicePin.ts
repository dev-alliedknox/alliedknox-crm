import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class DevicePin implements IValidateRequest{
  // REQUIRED
  public deviceUid: string;
  

  public constructor(data: any) {
    this.deviceUid = data.serial || '';    

    this.validate();
  }

  public validate() {    
    let emptyDeviceUid: boolean = isNullOrUndefined(this.deviceUid) || (this.deviceUid == '');
    
    if (emptyDeviceUid) 
      throw new ValidateError('Serial is required.');          

  }

}

export default DevicePin;