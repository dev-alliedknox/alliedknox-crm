
class BlinkDeviceOut {  
    // REQUIRED
    public deviceUid: string[];
    public email: string;
    public tel: string;
    public message: string;   
    public interval: number;   
  
    
    public constructor(serial: string[], email: string, tel: string, msg: string, interval: number) {
      this.deviceUid = serial;   
      this.email = email;
      this.tel = tel;
      this.message = msg; 
      this.interval = interval;   
    }
  
  }
  
  export default BlinkDeviceOut;