import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";

class LockDeviceIn implements IValidateRequest{
  // REQUIRED
  public cpf: string;
  public imei: string;
  public messageId: number; 

  public constructor(data: any) {
    this.cpf = data.cpf || '';    
    this.imei = data.imei || '';    
    this.messageId = data.messageId;

    this.validate();
  }

  public validate() {    
    let emptyCPF: boolean = isNullOrUndefined(this.cpf) || (this.cpf == '');
    let emptyIMEI: boolean = isNullOrUndefined(this.imei) || (this.imei == '');
    let emptyMessageId: boolean = isNullOrUndefined(this.messageId);
    
    if (emptyCPF) {
      if (emptyIMEI) 
        throw new ValidateError('CPF or IMEI/SN is required.');
    }        

    if (emptyMessageId) 
      throw new ValidateError('MessageId is required.');    

  }

}

export default LockDeviceIn;