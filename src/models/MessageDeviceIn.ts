import IValidateRequest from "../interfaces/IValidateRequest";
import { ValidateError } from "../errors/ValidateError";
import { isNullOrUndefined } from "util";


class MessageDeviceIn implements IValidateRequest{  
  // REQUIRED
  public cpf: string;
  public imei: string;
  public dtHrAuth: string;
  public valorCompra: number;
  public messageId: number;      
  public dtVenc: string;  
  public parcelaAberta: number; 


  public constructor(data: any) {
    this.cpf = data.cpf || '';
    this.imei = data.imei || '';
    this.dtHrAuth = data.dtHrAuth;
    this.valorCompra = data.valorCompra;
    this.messageId = data.messageId;
    this.dtVenc = data.dtVenc;
    this.parcelaAberta = data.parcelaAberta;    

    this.validate();
  }


  public validate() {    
    let emptyCPF: boolean = isNullOrUndefined(this.cpf) || (this.cpf == '');
    let emptyIMEI: boolean = isNullOrUndefined(this.imei) || (this.imei == '');
    let emptyMessageId: boolean = isNullOrUndefined(this.messageId);
    

    if (emptyCPF) {
      if (emptyIMEI) 
        throw new ValidateError('CPF or IMEI/SN is required.');
    }

    if (emptyMessageId) 
      throw new ValidateError('MessageId is required.');                                 

  }

}

export default MessageDeviceIn;