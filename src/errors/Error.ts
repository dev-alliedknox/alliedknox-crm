class Error {

  code: Number;
  message: String;
  reason: String;
}

export { Error };