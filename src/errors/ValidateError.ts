import { Err } from 'allied-kernel';

class ValidateError extends Err {

    public constructor(message: string) {
        
        super (message, 400, 'validate-error');
    }
}

export { ValidateError }