import { Err } from 'allied-kernel';

class AuthError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'Auth-error');
    }

} 

export { AuthError };