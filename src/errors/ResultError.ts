import { Error } from './Error';

class ResultError {
    
    result: String;
    error: Error;
}

export { ResultError };