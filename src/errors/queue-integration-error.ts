
import { Err } from 'allied-kernel';

class QueueIntegrationError extends Err {

    public constructor(message: string, statusCode: number) {

        super(message, statusCode, 'queue-integration-error');
    }
}

export default QueueIntegrationError;