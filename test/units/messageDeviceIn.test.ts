import {expect, should, request, assert} from '../init'
import 'mocha';
import MessageDeviceIn from '../../src/models/MessageDeviceIn';

describe('MessageDeviceInTestOK', () => {     

    it('Requisição deve ter os parametros obrigatórios (cpf).', () => {        
        let inputData = {
            "cpf": '12345678910',             
            "messageId": 1            
        };            
        
        expect(() => {
            new MessageDeviceIn(inputData)
        }).not.to.throw(Error);       
    });
    
    it('Requisição deve ter os parametros obrigatórios (imei).', () => {        
        let inputData = {
            "imei": '656565656565',             
            "messageId": 1            
        };            
        
        expect(() => {
            new MessageDeviceIn(inputData)
        }).not.to.throw(Error);       
    });

});


describe('MessageDeviceInTestNotOK', () => {

    it('Requisição sem parâmetros obrigatórios (cpf e imei).', () => {  
        let inputData = { };        
        
        expect(() => {            
            new MessageDeviceIn(inputData)
        }).to.throw(Error);        
    });

    it('Requisição sem parâmetro obrigatório (messageId).', () => {      
        let inputData = { "cpf": '12345678910', "dtHrAuth": '20190812', "valorCompra": 987.65 };       
        
        expect(() => {            
            new MessageDeviceIn(inputData)
        }).to.throw(Error);      
    });        

});
