import {expect} from '../init'
import 'mocha';
import UnlockDeviceIn from '../../src/models/UnlockDeviceIn';

describe('UnlockDeviceInTestOK', () => {     

    it('Requisição deve ter o parametro obrigatório (cpf).', () => {        
        let inputData = { "cpf": '12345678910' };            
        
        expect(() => {
            new UnlockDeviceIn(inputData)
        }).not.to.throw(Error);       
    });
    
    it('Requisição deve ter o parametro obrigatório (imei).', () => {        
        let inputData = { "imei": '98989899898' };            
        
        expect(() => {
            new UnlockDeviceIn(inputData)
        }).not.to.throw(Error);       
    });

});


describe('UnlockDeviceInTestNotOK', () => {

    it('Requisição sem parâmetros obrigatórios (cpf e imei).', () => {  
        let inputData = '';        
        
        expect(() => {            
            new UnlockDeviceIn(inputData)
        }).to.throw(Error);        
    }); 

});
