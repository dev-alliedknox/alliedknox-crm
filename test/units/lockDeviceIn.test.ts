import {expect} from '../init'
import 'mocha';
import LockDeviceIn from '../../src/models/LockDeviceIn';

describe('LockDeviceInTestOK', () => {     

    it('Requisição deve ter o parametro obrigatório (cpf).', () => {        
        let inputData = { "cpf": '12345678910', "messageId": 1 };            
        
        expect(() => {
            new LockDeviceIn(inputData)
        }).not.to.throw(Error);       
    });
    
    it('Requisição deve ter o parametro obrigatório (imei).', () => {        
        let inputData = { "imei": '2323232323232', "messageId": 1 };            
        
        expect(() => {
            new LockDeviceIn(inputData)
        }).not.to.throw(Error);       
    });

    it('Requisição deve ter o parametro obrigatório (messageId).', () => {        
        let inputData = { "imei": '2323232323232', "messageId": 1 };            
        
        expect(() => {
            new LockDeviceIn(inputData)
        }).not.to.throw(Error);       
    });

});


describe('LockDeviceInTestNotOK', () => {

    it('Requisição sem parâmetros obrigatórios (cpf e imei).', () => {  
        let inputData = '';        
        
        expect(() => {            
            new LockDeviceIn(inputData)
        }).to.throw(Error);        
    }); 

    it('Requisição sem parâmetro obrigatório (messageId).', () => {  
        let inputData = { "cpf": '2323232323232' };      
        
        expect(() => {            
            new LockDeviceIn(inputData)
        }).to.throw(Error);        
    }); 

});
