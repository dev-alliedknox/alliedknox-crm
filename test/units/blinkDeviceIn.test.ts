import {expect, should, request, assert} from '../init'
import 'mocha';
import BlinkDeviceIn from '../../src/models/BlinkDeviceIn';

describe('BlinkDeviceInTestOK', () => {     

    it('Requisição deve ter os parametros obrigatórios (cpf).', () => {        
        let inputData = {
            "cpf": '12345678910',             
            "messageId": 1,            
            "blinkInterval": 60
        };            
        
        expect(() => {
            new BlinkDeviceIn(inputData)
        }).not.to.throw(Error);       
    });
    
    it('Requisição deve ter os parametros obrigatórios (imei).', () => {        
        let inputData = {
            "imei": '4545454545455454',             
            "messageId": 1,            
            "blinkInterval": 60
        };            
        
        expect(() => {
            new BlinkDeviceIn(inputData)
        }).not.to.throw(Error);       
    });

});


describe('BlinkDeviceInTestNotOK', () => {

    it('Requisição sem parâmetros obrigatórios (cpf e imei).', () => {  
        let inputData = { };        
        
        expect(() => {            
            new BlinkDeviceIn(inputData)
        }).to.throw(Error);        
    });   

    it('Requisição sem parâmetro obrigatório (messageId).', () => {      
        let inputData = { "cpf": '12345678910', "dtHrAuth": '20190812', "valorCompra": 987.65 };       
        
        expect(() => {            
            new BlinkDeviceIn(inputData)
        }).to.throw(Error);      
    });        

    it('Requisição sem parâmetro obrigatório (blinkInterval).', () => {      
        let inputData = {
            "cpf": '12345678910',             
            "messageId": 1            
        };       
        
        expect(() => {            
            new BlinkDeviceIn(inputData)
        }).to.throw(Error);      
    });  

    it('Requisição com parâmetro inválido (blinkInterval).', () => {      
        let inputData = {
            "cpf": '12345678910', 
            "messageId": 1,
            "blinkInterval": 99999
        };       
        
        expect(() => {            
            new BlinkDeviceIn(inputData)
        }).to.throw(Error);      
    });  

});
